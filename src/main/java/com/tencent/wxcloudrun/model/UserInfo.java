package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户信息
 *
 * @author yupili
 **/
@Data
public class UserInfo implements Serializable {
  private static final long serialVersionUID = -3598932937566896192L;
  private String province;
  private String nickName;
  private String language;
  private Integer gender;
  private String country;
  private String city;
  private String avatarUrl;
}
