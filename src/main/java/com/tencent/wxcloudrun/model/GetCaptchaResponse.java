package com.tencent.wxcloudrun.model;

import lombok.Data;

import java.io.Serializable;

/**
 * GetCaptchaResponse
 *
 * @author yupili
 **/
@Data
public class GetCaptchaResponse implements Serializable {
  private static final long serialVersionUID = 4200328781716962658L;
  private boolean result;
  private String captcha;
}
